#! /usr/bin/python
import sys

def update_state_before(line, state):
    if line[0:4] == 'diff':
        return 'Diff'
    if state == 'Header':
        return 'Header'
    if state == 'Stats':
        return 'Stats'
    if line[0:3] == '+++':
        return 'AddFile'
    if line[0:3] == '---':
        return 'RemoveFile'
    if line[0] == '+':
        return 'Add'
    if line[0] == '-':
        return 'Remove'
    if line[0:5] == 'index':
        return 'Index'
    if line[0:2] == '@@':
        return 'Position'
    return 'Normal'


def update_state_after(line, state):
    if state == 'Header' and line[0:3] == '---':
        return 'Stats'
    if state == 'Stats':
        return 'Stats'
    if state == 'Header':
        return 'Header'
    return 'Normal'

def colorize(line, state):
    green_color = '\033[92m'
    red_color = '\033[91m'
    yellow_color = '\033[93m'
    normal_color = '\033[0m'
    l = line.rstrip('\n')
    if state == 'Diff':
        return '%s%s%s' % (yellow_color, l, normal_color)
    if state in ['Header', 'Normal']:
        return l
    if state in ['Add', 'AddFile']:
        return '%s%s%s' % (green_color, l, normal_color)
    if state in ['Remove', 'RemoveFile']:
        return '%s%s%s' % (red_color, l, normal_color)
    if state in ['Index', 'Position', 'Stats']:
        return '%s%s%s' % (yellow_color, l, normal_color)

def color_patch(f):
    state = 'Header'
    for line in f:
        state = update_state_before(line, state)
        l = colorize(line, state)
        print l
        state = update_state_after(line, state)

def main():
    if (len(sys.argv) < 2) or ('-h' in sys.argv):
        print 'usage: %s [filename|-]' % sys.argv[0]
        sys.exit(1)
    filename = sys.argv[1]
    if filename == '-':
        f = sys.stdin
    else:
        f = open(filename, 'r')
    color_patch(f)
    f.close()
    return 0

if __name__ == "__main__":
    main()
