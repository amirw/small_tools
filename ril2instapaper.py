#! /usr/bin/python
import urllib
import urllib2
import json
import sys

def get_ril_credentials():
    username = raw_input('RIL Username: ')
    password = raw_input('RIL Password: ')
    apikey = raw_input('RIL API Key: ')
    return {'username' : username,
            'password' : password,
            'apikey'   : apikey}

def check_ril_auth(credentials):
    url = "https://readitlaterlist.com/v2/auth"
    opts_encoded = urllib.urlencode(credentials)
    try:
        response = urllib2.urlopen(url, opts_encoded, timeout=5)
        data = response.read()
    except:
        return False
    if data == '200 OK':
        return True
    print data
    return False

def get_ril_list(credentials):
    ril_list = []
    url = "https://readitlaterlist.com/v2/get"
    opts = {'username' : credentials['username'],
            'password' : credentials['password'],
            'apikey' : credentials['apikey'],
            'state' : 'unread'}
    opts_encoded = urllib.urlencode(opts)
    try:
        response = urllib2.urlopen(url, opts_encoded, timeout=5)
        data = response.read()
    except:
        return []
    ril_data = json.loads(data)
    keys = ril_data['list'].keys()
    for key in keys:
        item = ril_data['list'][key]
        item_dict = {'url' : item['url'].encode('utf-8'), 'title' : item['title'].encode('utf-8')}
        ril_list.append(item_dict)
    return ril_list

def get_instapaper_credentials():
    username = raw_input('Instapaper Username: ')
    password = raw_input('Instapaper Password: ')
    return {'username' : username,
            'password' : password}

def check_instapaper_auth(credentials):
    url = "https://instapaper.com/api/authenticate"
    opts_encoded = urllib.urlencode(credentials)
    try:
        response = urllib2.urlopen(url, opts_encoded, timeout=5)
        data = response.read()
    except:
        return False
    if data == '200':
        return True
    return False

def push_list_to_instapaper(credentials, url_list, ril_credentials):
    url = "https://www.instapaper.com/api/add"
    for idx,item in enumerate(url_list):
        opts = {'username' : credentials['username'],
                'password' : credentials['password'],
                'url' : item['url'],
                'title' : item['title']}
        opts_encoded = urllib.urlencode(opts)
        try:
            response = urllib2.urlopen(url, opts_encoded, timeout=5)
            data = response.read()
            #delete_item_from_ril(ril_credentials, item)
        except:
            print 'Failed to add url  %s (%s) to Instapaper' % (item['url'], item['title'])
            sys.exit(1)
        if data == '201':
            print '%d / %d done' % (idx + 1, len(url_list))
        else:
            print 'Failed to add url  %s (%s) to Instapaper' % (item['url'], item['title'])
            sys.exit(1)

def delete_item_from_ril(credentials, item):
    json_object = '{"0" : {"url":"%s"} }' % item['url']
    opts = {'username' : credentials['username'],
            'password' : credentials['password'],
            'apikey' : credentials['apikey'],
            'read' : json_object}
    opts_encoded = urllib.urlencode(opts)
    url = "https://readitlaterlist.com/v2/send"
    urllib2.urlopen(url, opts_encoded, timeout=5)

def main():
    ril_credentials = get_ril_credentials()
    print 'Authenticating RIL'
    if not check_ril_auth(ril_credentials):
        print 'Error communicating with RIL. Please check credentials and try again'
        return
    print 'RIL Authentication OK'
    instapaper_credentials = get_instapaper_credentials()
    print 'Authenticating Instapaper'
    if not check_instapaper_auth(instapaper_credentials):
        print 'Error communicating with Instapaper. Please check credentials and try again'
        return
    print 'Instapaper Authentication OK'
    print 'Retrieving RIL list'
    ril_list = get_ril_list(ril_credentials)
    print 'Retrieved %d items from RIL' % len(ril_list)
    if len(ril_list) == 0:
        print 'No items to import'
        return
    print 'Uploading items to instapaper'
    push_list_to_instapaper(instapaper_credentials, ril_list, ril_credentials)
    print 'Done uploading items to instapaper'

if __name__ == "__main__":
    main()
